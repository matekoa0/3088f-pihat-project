# 3088F PiHat Project

The MicroHAT will be a pressure sensory device, designed to be used underwater. Using the relationship between pressure and depth underwater, the device will sense, record and display its depth up to 150m below the water’s surface. It will be attached to a Raspberry Pi Zero board and use the available hardware on this device.

Using Raspberry’s timing module, the device will take and store distance measurements at set time intervals. These recordings will be stored within an EEPROM/microSD card to be read on a computer. For on-site reading, there will be a small LCD screen as well as LEDs that light up and remain lit upon reaching certain depths. Thus, when the device is retrieved, the LEDs can be read to indicate a range of depth.

## Applications include:

### Divers
Notifies divers of their current depth.
LEDs act as a safety warning for set depths.
Records past depths traversed
Indicates oxygen required for future dives to reach particular depths.

### Research
Gathers data regarding the depth of observed oceanic regions.
Provides information on changing habitat and current conditions by showing the changes in depth and pressure at a constant device position.

### Dams
Water levels of dams are recorded and stored.
